#include <TaskOverInterrupt.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

TOIFunction everyTimeFunction = (TOIFunction)0;
const TOIFunction *tasks = 0;
const TOIFunction *currentTask = 0;

volatile uint8_t TOIFunctionId = 0;
volatile uint16_t TOIFunctionIdTime = 0xFFFF;

volatile uint8_t _TOIFMaxTimeId = 0;
volatile uint16_t _TOIFMaxTimeTime = 0;
volatile uint8_t _TOIFMaxTimeRId = 0;
volatile uint16_t _TOIFMaxTimeRTime = 0;

SIGNAL(TIMER1_COMPA_vect) {
  static uint8_t taskId = 0;
  uint16_t _TCNT1;
  if (everyTimeFunction != (TOIFunction)0) {
    everyTimeFunction();
  }
  if (tasks != (TOIFunction *)0) {
    TOIFunction taskFunction;
  reloadtask:
    memcpy_P(&taskFunction, currentTask, sizeof(TOIFunction));
    currentTask++;
    if (taskFunction == (TOIFunction)0x0000) {
      taskId = 0;
      currentTask = tasks;
      _TOIFMaxTimeRId = _TOIFMaxTimeId;
      _TOIFMaxTimeRTime = _TOIFMaxTimeTime;
      _TOIFMaxTimeTime = 0;
      _TOIFMaxTimeId = 0;
      goto reloadtask;
    }
    if (taskFunction != (TOIFunction)0xFFFF) { // Vynechany task
      taskFunction();
    }
    _TCNT1 = TCNT1;
    if (taskId == TOIFunctionId) {
      TOIFunctionIdTime = _TCNT1;
    }
    if (_TCNT1 >= _TOIFMaxTimeTime) {
      _TOIFMaxTimeTime = _TCNT1;
      _TOIFMaxTimeId = taskId;
    }
    taskId += 1;
  }
}

void TOIInit(uint16_t divider, TOIFunction everyTime,
             const TOIFunction newTasks[]) {
  TIMSK1 &= ~(1 << OCIE1A);
  tasks = newTasks;
  currentTask = tasks;
  everyTimeFunction = everyTime;
  // Timer1 je v modu 4, preddelicka je nastavena na 256. (Citac jede od 0 do
  // OCR1A a pak se resetuje);
  TCCR1A = 0x00;
  TCCR1B = (1 << WGM12) | (1 << CS12);
  OCR1A = divider;
  TIMSK1 |= (1 << OCIE1A);
}

uint16_t TOITime(uint8_t taskId) {
  if (taskId != TOIFunctionId) {
    uint8_t tmp = SREG;
    cli();
    TOIFunctionIdTime = 0xFFFF;
    TOIFunctionId = taskId;
    SREG = tmp;
  }
  uint8_t tmp = SREG;
  cli();
  uint16_t x = TOIFunctionIdTime;
  SREG = tmp;
  return x;
}

uint16_t TOIMaxTime(uint8_t *taskId) {
  *taskId = _TOIFMaxTimeRId;
  return _TOIFMaxTimeRTime;
}
