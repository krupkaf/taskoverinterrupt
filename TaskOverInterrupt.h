#ifndef TASKOVERINTERRUPT_H_
#define TASKOVERINTERRUPT_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define FREQUENCY2DIVIDER(f) ((uint16_t)(((float)F_CPU / 256.0) / (float)f))

typedef void (*TOIFunction)(void);

void TOIInit(uint16_t divider, TOIFunction everyTime,
             const TOIFunction newTasks[]);

//Mereni casu provadeni tasku. Cas je vracen v poctech tiku TCNT1.
uint16_t TOITime(uint8_t taskId);
uint16_t TOIMaxTime(uint8_t *taskId);

#ifdef __cplusplus
}
#endif

#endif
