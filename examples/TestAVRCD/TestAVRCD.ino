#include "TaskOverInterrupt.h"

volatile uint16_t aaa = 0;
volatile uint16_t bbb = 0;
volatile uint16_t ccc = 0;

void everyTime(void) {
        aaa++;
}

void task1(void) {
        bbb++;
}

void task2(void) {
        ccc++;
}

const TOIFunction PROGMEM taskFuns[] = {
        task1,
        task2,
        (TOIFunction)0xFFFF,
        (TOIFunction)0xFFFF,
        (TOIFunction)0
};

void setup() {
        Serial.begin(115200);
        TOIInit(FREQUENCY2DIVIDER(2), everyTime, taskFuns);
}

void loop() {
        Serial.print(F("Button:"));
        Serial.print(millis());
        Serial.print(' ');
        Serial.print(aaa);
        Serial.print(' ');
        Serial.print(bbb);
        Serial.print(' ');
        Serial.print(ccc);
        Serial.println();
        delay(200);
}
